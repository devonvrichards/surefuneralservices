<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Controller {


	public function index()
	{
        display_view('template', 'home', 'home', NULL);

	}

    public function client()
    {
        display_view('template', 'client', 'client', NULL);

    }
    public function consultant()
    {
        display_view('template', 'consultant', 'consultant', NULL);

    }
    public function zone()
    {
        display_view('template', 'location', 'zone', NULL);

    }
}
