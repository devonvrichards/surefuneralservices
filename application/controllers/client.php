<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client extends CI_Controller {


    public function add_client()
    {
        display_view('template', 'client', 'add_client', NULL);

    }

    public function insert_client()
    {


        $this->load->model("client_model", "client");

        $data = array(
            'name' => $this->input->post('name'),
            'surname' => $this->input->post('surname'),
            'id_number' => $this->input->post('id_number'),
            'address' => $this->input->post('address'),
            'gender' => $this->input->post('gender'),
            'contact_number' => $this->input->post('contact_number'),
            'zone_id' => $this->input->post('zone')
            );

        $client_found = $this->client->check_user($this->input->post('id_number'));

        if($client_found)
        {
            $data['message'] = "This client already exists on the system!";
            display_view('template', 'client', 'add_client', $data);
        }
        else
        {
            $response = $this->client->create_client($data);

            if($response)
            {
                $data['client_id'] = $response;
                display_view('template', 'client', 'client_health', $data);
            }
        }

    }

    public function insert_health()
    {


        $this->load->model("client_model", "client");

        $data = array(
            'cancer' => $this->input->post('cancer'),
            'heart_disease' => $this->input->post('heart_disease'),
            'blood_pressure' => $this->input->post('blood_pressure'),
            'diabetes' => $this->input->post('diabetes'),
            'arthritis' => $this->input->post('arthritis'),
            'tb' => $this->input->post('tb'),
            'asthma' => $this->input->post('asthma'),
            'hiv_aids' => $this->input->post('hiv_aids'),
            'other' => $this->input->post('other')
        );


        $response = $this->client->insert_health($data);

        if($response)
        {
            $message = "Client successfully added to system!";
            $this->list_client($message);
        }
    }

    public function list_client($message = NULL)
    {
        $this->load->model("client_model", "client");
        $clients = $this->client->get_client();
        $data['clients'] = $clients;
        $data['message'] = $message;
        display_view('template', 'client', 'list_clients', $data);
    }

    public function view_client($client_id)
    {

        $this->load->model("client_model", "client");
        $personal_details = $this->client->get_client_personal_details($client_id);
        $health_details = $this->client->get_client_health_details($client_id);

        $data['personal'] = $personal_details;
        $data['health'] = $health_details;
        display_view('template', 'client', 'view_client', $data);
    }

    public function find_client()
    {
        display_view('template', 'client', 'find_client', NULL);
    }



}
