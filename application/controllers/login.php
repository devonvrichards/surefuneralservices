<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
        display_view_login('template_login', 'login', 'index', NULL);


	}

	public function validate_user()
	{
		$post_values = $this->input->post();

		$username = $post_values['username'];
		$password = $post_values['password'];


		$this->load->model('login_model', 'login');
		$users = $this->login->validate_user($username,$password);

		if(empty($users))
		{
			redirect('/login/index/error');
		}
		else
		{

            $this->load->library('session');
			$this->session->set_userdata('user_id',$users['id']);
            $this->session->set_userdata('name',$users['name']." ".$users['surname']);


            redirect('site');
		}
	}
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */
