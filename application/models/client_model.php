<?php

class Client_model extends CI_Model
{

    function create_client($data)
    {
        if($this->db->insert('client', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    function insert_health($data)
    {
        if($this->db->insert('health', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    function get_client()
    {
        $q = $this
            ->db
            ->select('*')
            ->from('client')
            ->get()
            ->result_array();
        return $q;
    }

    function get_client_personal_details($client_id)
    {
        $q = $this
            ->db
            ->select('*')
            ->from('client')
            ->where('id',$client_id)
            ->get()
            ->row_array();
        return $q;
    }

    function get_client_health_details($client_id)
    {
        $q = $this
            ->db
            ->select('*')
            ->from('health')
            ->where('client_id',$client_id)
            ->get()
            ->row_array();
        return $q;
    }
    function check_user($id_number)
    {
        $q = $this
            ->db
            ->select('*')
            ->from('client')
            ->where('id_number',$id_number)
            ->get()
            ->row_array();
        return $q;
    }


}