<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('is_empty'))
{
	function is_empty()
	{
        $CI = &get_instance();
        $cart = $CI->userdata('cart');

        //var_dump($cart);die();
        if($cart != false)
            return false;
        else
            return true;
	}
}

if ( ! function_exists('clear_cart'))
{
	function clear_cart()
	{
        $CI = &get_instance();
        $CI->unset_userdata('cart');
	}
}

if ( ! function_exists('get_cart'))
{
	function get_cart()
	{
        $CI = &get_instance();
        if(is_empty())
        {
            return false;
        }
        else
        {
            $cart_items = $CI->userdata('cart');
            return $cart_items;
        }
	}
}

if ( ! function_exists('cart_total'))
{
	function cart_total()
	{
        $CI = &get_instance();
        if(is_empty())
        {
            return 0;
        }
        else
        {
            $cart_items = $CI->userdata('cart');
            $total = 0;
            foreach($cart_items as $item)
            {
                $item_total = 0;
                for($i = 0; $i<$item['quantity']; $i++)
                {
                    $item_total += $item['amount'];
                }
                $total +=  $item_total;
            }
            return $total;
        }
	}
}

/* End of file view_helper.php */