<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('display_view'))
{
	function display_view($header, $view_folder, $main_content, $data = NULL)
	{
		$CI =& get_instance();

		$data['header'] = $header;
		$data['app_name'] = 'Youth Café';

		if($view_folder == NULL)
		{
			$data['main_content'] = $main_content;
		}
		else
		{
			$data['main_content'] = $view_folder . '/' . $main_content;
		}

		$CI->load->view('includes/template', $data);
	}
	function display_view_login($header, $view_folder, $main_content, $data = NULL)
	{
		$CI =& get_instance();

		$data['header'] = $header;

		if($view_folder == NULL)
		{
			$data['main_content'] = $main_content;
		}
		else
		{
			$data['main_content'] = $view_folder . '/' . $main_content;
		}

		$CI->load->view('includes/template_login', $data);
	}
	function browser_detection(){

		if (!empty($_SERVER['HTTP_CLIENT_IP'])){
			//check ip from share internet
			$ip=$_SERVER['HTTP_CLIENT_IP'];
		}else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
			//to check ip is pass from proxy
			$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
		}else{
			$ip=$_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}
}

/* End of file view_helper.php */