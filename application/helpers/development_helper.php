<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('dump'))
{
    function dump($data)
    {
		echo '<pre>';
		print_r($data);
		echo '<pre>';
    }
}

/* End of file view_helper.php */
/* Location: ./system/helpers/view_helper.php */