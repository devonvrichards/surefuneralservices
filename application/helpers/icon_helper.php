<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('assign_icon')) {

    function assign_icon($service_type_name)
    {
       switch($service_type_name)
       {
           case 'Programmes':
               echo '<i class="fa fa-pencil-square-o"></i>';
               break;

           case 'Workshops':
               echo '<i class="fa fa-wrench"></i>';
               break;

           case 'Sessions':
               echo '<i class="fa fa-laptop"></i>';
               break;

           case 'Events':
               echo '<i class="fa fa-calendar"></i>';
               break;

           case 'Community':
               echo '<i class="fa fa-users"></i>';
               break;

           case 'Information':
               echo '<i class="fa fa-info-circle"></i>';
               break;

           case 'Internet':
               echo '<i class="fa fa-cloud"></i>';
               break;

           case 'Cafe Item':
               echo '<i class="fa fa-cutlery"></i>';
               break;

            default:
               echo '<i class="fa fa-th-list"></i>';

       }
    }
}

    if (!function_exists('item_icons')) {

        function item_icons($service_type_name)
        {
            switch($service_type_name)
            {
                case 'Programmes':
                    echo '<i class="fa fa-pencil-square-o fa-5x"></i>';
                    break;

                case 'Workshops':
                    echo '<i class="fa fa-wrench fa-5x"></i>';
                    break;

                case 'Sessions':
                    echo '<i class="fa fa-laptop fa-5x"></i>';
                    break;

                case 'Events':
                    echo '<i class="fa fa-calendar fa-5x"></i>';
                    break;

                case 'Community':
                    echo '<i class="fa fa-users fa-5x"></i>';
                    break;

                case 'Information':
                    echo '<i class="fa fa-info-circle fa-5x"></i>';
                    break;

                case 'Internet':
                    echo '<i class="fa fa-cloud fa-5x"></i>';
                    break;

                case 'Cafe Item':
                    echo '<i class="fa fa-cutlery fa-5x"></i>';
                    break;

                default:
                    echo '<i class="fa fa-th-list fa-5x"></i>';


            }
        }

    }
