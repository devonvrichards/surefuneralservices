

<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <i class="fa fa-users"></i>Client <small>Information</small>
            </h1>
        </div>
    </div>

    <ol class="breadcrumb">

        <center>
            <li>
                <i class="fa fa-dashboard"></i><b>Viewing Client SF<?php echo $personal['id']?></b>
            </li></center>
    </ol>
    <!-- /.row -->
    <a href="<?php echo base_url('client/list_client');?>"><div class="btn btn-info">Back</div></a>
    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
              <div class="well" style="height: 720px">
                  <h2><strong>Personal Details</strong></21>
                  <hr>
                  <div class="col-lg-6">
                      <h4><b>Name: </b></h4>
                      <h4><b>Surname: </b></h4>
                      <h4><b>ID Number: </b></h4>
                      <h4><b>Gender: </b></h4>
                      <h4><b>Address: </b></h4>
                      <h4><b>Contact Number: </b></h4>
                  </div>

                  <div class="col-lg-6">
                      <center>
                      <h4><?php echo $personal['name']?></h4>
                      <h4><?php echo $personal['surname']?></h4>
                      <h4><?php echo $personal['id_number']?></h4>
                      <h4><?php echo $personal['gender']?></h4>
                      <h4><?php echo $personal['address']?></h4>
                      <h4><?php echo $personal['contact_number']?></h4>
                      </center>  <br><br>
                  </div>

                  <h2><strong>Health Details</strong></21>
                  <hr>
                  <?php

                  ?>
                  <div class="col-lg-6">
                      <h4><b>Cancer: </b></h4>
                      <h4><b>Heart Disease: </b></h4>
                      <h4><b>Low/High Blood Pressure: </b></h4>
                      <h4><b>Diabetes: </b></h4>
                      <h4><b>Asthma: </b></h4>
                      <h4><b>Arthritis: </b></h4>
                      <h4><b>TB: </b></h4>
                      <h4><b>HIV/AIDS: </b></h4>
                      <h4><b>Other: </b></h4>
                  </div>

                  <div class="col-lg-6">
                      <center>
                          <h4><?php if($health['cancer'] == 1){echo "Yes";}else {echo "No";}?></h4>
                          <h4><?php if($health['heart_disease'] == 1){echo "Yes";}else {echo "No";}?></h4>
                          <h4><?php if($health['blood_pressure'] == 1){echo "Yes";}else {echo "No";}?></h4>
                          <h4><?php if($health['diabetes'] == 1){echo "Yes";}else {echo "No";}?></h4>
                          <h4><?php if($health['asthma'] == 1){echo "Yes";}else {echo "No";}?></h4>
                          <h4><?php if($health['arthritis'] == 1){echo "Yes";}else {echo "No";}?></h4>
                          <h4><?php if($health['tb'] == 1){echo "Yes";}else {echo "No";}?></h4>
                          <h4><?php if($health['hiv_aids'] == 1){echo "Yes";}else {echo "No";}?></h4>
                          <h4><?php
                              if($health['other'] == !NULL)
                              {
                                  echo $health['other'];
                              }
                              else
                              {
                                  echo "None";
                              }

                              ?></h4>
                      </center>
                  </div>
                  <?php

                  ?>
              </div>

            </div>
        </div>

        <div class="col-lg-3"></div>
    </div>


</div>
<!-- /.container-fluid -->


