

<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <i class="fa fa-users"></i>Clients <small>Viewing all clients</small>
            </h1>
        </div>
    </div>
    <?php

    if(isset($message))
    {
        echo'<div class="alert alert-success alert-dismissable">'.$message.'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>';

    }

    ?>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-10">
            <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                    <tr>
                        <th>Client ID</th>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Contact No.</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    if(empty($clients))
                    {
                       echo"<div class='alert alert-danger'><strong> There are no clients on the system at the moment!</strong> </div>";
                    }
                    else
                        {
                            foreach($clients as $client)
                            {
                                echo'<tr>';
                                echo'<td>SF'.$client['id'].'</td>';
                                echo'<td>'.$client['name'].'</td>';
                                echo'<td>'.$client['surname'].'</td>';
                                echo'<td>'.$client['contact_number'].'</td>';
                                echo'<td><a href="view_client/'.$client['id'].'">View details</a></td>';
                                echo' </tr>';
                            }
                        }


                    ?>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="col-lg-1"></div>
    </div>


</div>
<!-- /.container-fluid -->


