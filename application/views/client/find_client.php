

<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <i class="fa fa-users"></i>Client <small>Search</small>
            </h1>
        </div>
    </div>
    <ol class="breadcrumb"><center>
            <li>
                <i class="fa fa-filter"></i><b>Search Filter</b>
            </li>
    </ol></center>
    <!-- /.row -->

    <center>
    <div class="form-group">

        <label>Please select an option below before clicking search:</label>
        <br>
        <label class="radio-inline">
            <input type="radio" name="optionsRadiosInline" id="optionsRadiosInline1" value="option1" checked="">Client ID
        </label>
        <label class="radio-inline">
            <input type="radio" name="optionsRadiosInline" id="optionsRadiosInline2" value="option2">ID Number
        </label>
        <label class="radio-inline">
            <input type="radio" name="optionsRadiosInline" id="optionsRadiosInline3" value="option3">Name/Surname
        </label>
    </div></center>
    <hr>
    <div class="row">
        <div class="form-group input-group">
            <input type="text" class="form-control">
            <span class="input-group-btn"><button class="btn btn-default" type="button"><i class="fa fa-search"></i></button></span>
        </div>
        <div class="col-lg-3"></div>
        <div class="col-lg-6">

        </div>

        <div class="col-lg-3"></div>
    </div>


</div>
<!-- /.container-fluid -->


