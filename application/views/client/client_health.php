

<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <i class="fa fa-user-plus"></i> Add Client <small>Health Information</small>
            </h1>
        </div>
    </div>
    <ol class="breadcrumb"><center>
            <li>
                <b>Step 2 of 2</b>
            </li></center>
    </ol>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <?php echo form_open_multipart('client/insert_health');?>

    <h4><b>Does the client suffer from any of the following health related issues?</b></h4>
            <hr>
            <div class="form-group">
                <label>Cancer</label><br>
                <label class="radio-inline">
                    <input type="radio" name="cancer" id="cancer" value="true" >Yes
                </label>
                <label class="radio-inline">
                    <input type="radio" name="cancer" id="cancer" value="false" checked>No
                </label>

            </div>
    <br>
    <div class="form-group">
        <label>Heart Disease</label><br>
        <label class="radio-inline">
            <input type="radio" name="heart_disease" id="heart_disease" value="true" >Yes
        </label>
        <label class="radio-inline">
            <input type="radio" name="heart_disease" id="heart_disease" value="false" checked>No
        </label>

    </div>
    <br>
    <div class="form-group">
        <label>High/Low Blood Pressure</label><br>
        <label class="radio-inline">
            <input type="radio" name="blood_pressure" id="blood_pressure" value="true" >Yes
        </label>
        <label class="radio-inline">
            <input type="radio" name="blood_pressure" id="blood_pressure" value="false" checked>No
        </label>

    </div>
    <br>
    <div class="form-group">
        <label>Diabetes</label><br>
        <label class="radio-inline">
            <input type="radio" name="diabetes" id="diabetes" value="true" >Yes
        </label>
        <label class="radio-inline">
            <input type="radio" name="diabetes" id="diabetes" value="false" checked>No
        </label>

    </div>
<br>
    <div class="form-group">
        <label>Arthritis</label><br>
        <label class="radio-inline">
            <input type="radio" name="arthritis" id="arthritis" value="true" >Yes
        </label>
        <label class="radio-inline">
            <input type="radio" name="arthritis" id="arthritis" value="false" checked>No
        </label>

    </div>
<br>
            <div class="form-group">
                <label>Asthma</label><br>
                <label class="radio-inline">
                    <input type="radio" name="asthma" id="asthma" value="true" >Yes
                </label>
                <label class="radio-inline">
                    <input type="radio" name="asthma" id="asthma" value="false" checked>No
                </label>

            </div>
            <br>

    <div class="form-group">
        <label>Tuberculosis</label><br>
        <label class="radio-inline">
            <input type="radio" name="tb" id="tb" value="true" >Yes
        </label>
        <label class="radio-inline">
            <input type="radio" name="tb" id="tb" value="false" checked>No
        </label>

    </div>
    <br>
    <div class="form-group">
        <label>HIV/Aids</label><br>
        <label class="radio-inline">
            <input type="radio" name="hiv_aids" id="hiv_aids" value="true" >Yes
        </label>
        <label class="radio-inline">
            <input type="radio" name="hiv_aids" id="hiv_aids" value="false" checked>No
        </label>

    </div>
<br>
    <div class="form-group">
        <label>Other</label>
        <input class="form-control"  name="other">
        <p class="help-block">Please specify what the health issue is.</p>
    </div>
            <hr>
            <button style="float: right" type="submit" class="btn btn-default">Submit</button>

            </form>
        </div>

        <div class="col-lg-3"></div>
    </div>


</div>
<!-- /.container-fluid -->


