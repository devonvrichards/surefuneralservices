

<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <i class="fa fa-user-plus"></i> Add Client <small>Personal Information</small>
            </h1>
        </div>
    </div>
    <ol class="breadcrumb"><center>
        <li>
           <b>Step 1 of 2</b>
        </li></center>
    </ol>
    <!-- /.row -->
    <div class="row">
        <?php

        if(isset($message))
        {
            echo'<center><div class="alert alert-danger alert-dismissable">'.$message.'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div></center>';

        }

        ?>
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
                <?php echo form_open_multipart('client/insert_client');?>
                <div class="form-group">
                    <label>Name<star style="color: red">*</star></label>
                    <input class="form-control" required name="name">
                    <p class="help-block">Client First Name</p>
                </div>
                <div class="form-group">
                    <label>Surname<star style="color: red">*</star></label>
                    <input class="form-control" required name="surname">
                    <p class="help-block">Client Last Name</p>
                </div>
                <div class="form-group">
                    <label>ID Number<star style="color: red">*</star></label>
                    <input class="form-control" required MAXLENGTH="13" name="id_number">
                    <p class="help-block">Client Identification Number</p>
                </div>
                <div class="form-group">
                    <label>Address<star style="color: red">*</star></label>
                    <input class="form-control" required name="address">
                    <p class="help-block">Client Residential Address</p>
                </div>
                <div class="form-group">
                    <label>Contact Number<star style="color: red">*</star></label>
                    <input class="form-control" required name="contact_number" >
                    <p class="help-block">Client Home/Cell Number</p>
                </div>
                <div class="form-group">
                    <label>Gender<star style="color: red">*</star></label><br>
                    <label class="radio-inline">
                        <input type="radio" name="gender" id="gender" value="Male" >Male
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="gender" id="gender" value="Female">Female
                    </label>
                    <p class="help-block">Client Gender</p>
                </div>

                <div class="form-group">
                    <label>Zone<star style="color: red">*</star></label>
                    <select class="form-control" required="required" name="zone">
                        <option selected="selected" disabled="disabled">Select a Zone</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                    </select>
                    <p class="help-block">Client is living in the selected zone.</p>
                </div>
            <hr>
                <button style="float: right" type="submit" class="btn btn-default">Next Step</button>

            </form>
        </div>

        <div class="col-lg-3"></div>
    </div>


</div>
<!-- /.container-fluid -->


