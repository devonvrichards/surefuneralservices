

<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <i class="fa fa-user-plus"></i> Add Consultant<small>Details</small>
            </h1>
        </div>
    </div>
    <ol class="breadcrumb"><center>
            <li>
                <b>Step 1 of 1</b>
            </li></center>
    </ol>
    <!-- /.row -->
    <div class="row">
        <?php

        if(isset($message))
        {
            echo'<center><div class="alert alert-danger alert-dismissable">'.$message.'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div></center>';

        }

        ?>
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <?php echo form_open_multipart('consultant/insert_consultant');?>
            <div class="form-group">
                <label>Name<star style="color: red">*</star></label>
                <input class="form-control" required name="name">
                <p class="help-block">Consultant First Name</p>
            </div>
            <div class="form-group">
                <label>Surname<star style="color: red">*</star></label>
                <input class="form-control" required name="surname">
                <p class="help-block">Consultant Last Name</p>
            </div>
            <div class="form-group">
                <label>Contact Number<star style="color: red">*</star></label>
                <input class="form-control" required name="contact_number">
                <p class="help-block">Consultant Contact Number</p>
            </div>

            <div class="form-group">
                <label>Email<star style="color: red">*</star></label>
                <input  type="email" class="form-control" required name="email">
                <p class="help-block">Consultant Email Address</p>
            </div>



            </form>
        </div>

        <div class="col-lg-3"></div>
    </div>


</div>
<!-- /.container-fluid -->


